#╔════════════════════════════════════════════════════════╗
#                           ALIASES
#╚════════════════════════════════════════════════════════╝
echo -e "\e[32m Configuring Aliases and Functions . . .\e[0m"
#═════════════════════════════════════════════════════════#
# TIME AND DATE
#═════════════════════════════════════════════════════════#

# displays the current time
alias now="date +"%T""

#═════════════════════════════════════════════════════════#
# DIRECTORIES
#═════════════════════════════════════════════════════════#

# list the name of the process matched with pgrep
alias pgrep="pgrep -l"

# replace ls with exa
alias ls=exa
alias ll="ls -al"
alias la="ls -a"
alias lsr="ls -alRGh"


function path ()
{
echo $PATH | tr ":" "\n"
}


# fixes for sloppy typing/lazinesd
alias lf=ls
alias lx=ls
alias ld=ls
alias lz=ls
alias vf=cd
alias ks=ls
alias jd=ls
alias kd=ls
alias vf=cd
alias cf=cd
alias xd=cd

#use batcat as bat
# alias bat=batcat

# adb fix
alias adb="adb.exe"

# fastboot fix
alias fastboot="fastboot.exe"

# scrcpy fix
alias scrcpy="/mnt/c/ProgramData/chocolatey/lib/scrcpy/tools/scrcpy.exe"
# note qtscrcpy.exe is in .winbin
alias qtscrcpy="QtScrcpy.exe"
# scrcpy gui
# alias scrcpy-gui="/mnt/c/Program\ Files\ \(x86\)/guiscrpy/guiscrcpy.exe"

# powershell as admin via wsl
# alias gsudo="gsudo.exe"

# choco fix
# alias choco="gsudo choco.exe ${@}"
alias choco="choco.exe"
alias gsudo="gsudo.exe"
alias choco-install="gsudo choco install ${@}"


#longlist a directory, by page
#lo [directoryname]
function lo () {
    if [ -d "$1" ] ; then
        ls - al -I=.git --git-ignore --color-scale --color=always "$1" | less
    else
        ls -al -I=.git --git-ignore --color-scale --color=always  $(pwd) | less
    fi
}

# Same as above but recursive
function llr () {
    if [ -d "$1" ] ; then
        ls -alR -I=.git  --git-ignore --color-scale --color=always "$1" | less
    else
        ls -alR -I=.git --git-ignore --color-scale --color=always $(pwd) | less
    fi
}

#list sort by date
alias lld="ll -l  --sort modified"

#list sort by size
alias lls="ls -alh --sort size"


#list only directories (uses above ls aliases as base)
alias lsdir="ls -d"
alias lldir="ll -D"
alias ladir="la -D"
alias llrdir="llr -D"
alias lsrdir="lsr -D"
alias llddir="lld -D"
alias llsdir="lls -D"

#Same as above but tree view
alias lsdirt="ladir -T"
alias lldirt="lldir -T"
alias ladirt="ladir -T"
alias llrdirt="llrdir -T"
alias lsrdirt="lsrdir -T"
alias llddirt="llddir -T"
alias llsdirt="llsdir -T"

# Open windows symlink app
alias symlinker='Symlink-Creator.exe'

# Open PDFXEdit
alias pdf="PDFXEdit.exe"

# Quick french ruled template in browser (to print)
alias pp="msedge.exe file:///C:/Users/jared/Documents/_SCRIPTS/_STATIONARY/FrenchRuledTemplate.pdf"

# open windows sublime text with arguments (supported)
alias sublime="sublime_text.exe"
alias quicksublime="cddc && sublime ./"

# make file and open in sublime (quickly)
function quickfile ()
{
    sublime $1

}
alias quickfile="quickfile"

# open android studio
alias andstd="studio64.exe"
alias studio64="studio64.exe"

# go to ...
alias cddt="cd /mnt/c/Users/jared/desktop"
alias cdldt="cd ~/desktop"
alias cddc="cd /mnt/c/Users/jared/documents"
alias cdldc="cd ~/documents && ls -a"
alias cddl="cd /mnt/c/Users/jared/downloads"
alias cdldl="cd ~/downloads"
alias cdcw="cddc && cd coursework"
alias cdrepos="cddc && cd _REPOS"
alias cdcad="cddc && cd _CAD"
alias cdscripts="cddc && cd _SCRIPTS"
alias cdprojects="cddc && cd _PROJECTS"
alias cdsublime="cddc && cd _SUBLIME"
alias cdroms="cddc && cd _ROMS"
alias cdlists="cdscripts && cd _LISTS"
alias cdulb="cd /usr/local/bin/ && ls -a"
alias cdbin="cd $HOME/bin/ && ls -a"
alias cdcrx="cd /mnt/c/Users/jared/documents/_CHROMEEXT"
alias cdarc="cddc && cd _ARC"
alias cdbash="cdscripts && cd _BASH"
alias cdcheats="cdscripts && cd _CHEATSHEETS-LIFEHACKS"
alias cdhacks="cdcheats"
alias cdjava="cdscripts && cd _JAVA"
alias cdpwa="cdscripts && cd _PWA"
alias cdedu="cd ${HOME}/bin/edu/princeton/cs/algs4"
alias cdalgs4="cdedu"
alias cdgl="cdscripts && cd _GL"
alias cdcmds="cdscripts && cd _CMDS"
alias cdapi="cdprojects && fd api -t d -d 1"
# go vms
alias cdvms="cddc && cd Virtual\ Machines"

# go to unity projects
alias cdunity="cddc && cd _UNITY && ls -a"
# go to droidscript master (submodule) repository
alias cdds="cddc && cd _DROIDSCRIPT"

# open droidscript master (submodule) repository
alias opends="cdds && qexp"

# go to cs460 graphics folder
alias cdgfx="cddc && cd _COURSEWORK-REPOS/cs460student"

# go to node_modules (global modules)
alias cdnode="cd /usr/lib/node_modules"

# go to homepage repo
alias cdweb="cddc && cd _HLTDEV-HOME/home"

# open homepage repo folder
alias openweb="cdweb && qexp"

# go to jterm repo
alias cdjterm="cddc && cd _COURSEWORK-REPOS/jterm"

# open jterm repo
alias openjterm="cdjterm && qexp"
function cdclass ()
{
    cdcw
    cd $1/$2
}

# open cheat sheets / lifehacks in scripts repo
alias opencheats="cdcheats && qexp"
alias openhacks="opencheats"

function mkclass ()
{

    courseName="${1}"
    cdcw
    mkdir "${courseName}"
    cd "${courseName}"
    mkdir "projects"
    mkdir "homework"
    mkdir "lectures"
    mkdir "files"
    mkdir "other"
    ls
}


# open a class
function cwgo ()
{

    courseName=$1
    if [[ ! -f $2 ]]; then
        folderName=$2
    else
        folderName="."
    fi

    # folderName=$2

    cdcw
    dirCheck="${PWD}/${courseName}/${folderName}"

    if [ -d $dirCheck ]
    then
        cd $dirCheck && ls
    else
        echo "Error: class does not exists." && ls
    fi
}

function cwopen ()
{
    courseName=$1

    cdcw
    cd $courseName
    qexp
}

# remote learning
# open a class
# open a class
function cwremote ()
{
    courseName=$1

    case $courseName in
        cs410 )
            echo -e "\e[32m\nStarting ${courseName} remote session . . . \n \e[0m"
            msedge.exe "https://umassboston.zoom.us/my/haehn"
        ;;
        cs461 )
            echo -e "\e[32m\nStarting ${courseName} remote session . . . \n \e[0m"
            msedge.exe "https://us.bbcollab.com/collab/ui/session/join/1e997bed02114294a5ea0d69615fd981"
            # "https://us.bbcollab.com/collab/ui/session/join/d79d2535bb2c4e9b8ac1d7d4e6036dae"
        ;;
        cs220 )
            echo -e "\e[32m\nStarting ${courseName} remote session . . . \n \e[0m"
            msedge.exe "https://us.bbcollab.com/collab/ui/session/join/91d0838355c8482cacab9a9b2e24bc70"
            # "https://us.bbcollab.com/collab/ui/session/join/b04673e2797c496591cbecef644102ee"
        ;;
        * )
            echo -e "\e[32m\nUsage: cwremote <coursename> \n \e[0m"
        ;;
    esac
}

# start budgie launcher
alias budgie="budgie-run-dialog"


# x410 shortcut
# start x410 xwindow server from wsl
function x410 ()
{
    cmd.exe /c "x410"
}

# two aliases to access x410 function
alias x410="/mnt/c/Program\ Files/WindowsApps/ChoungNetworksUS.X410_2.8.2.0_x64__vvzc8y2tzcnsr/X410/X410.exe"
alias startxserver="x410"

# open ..
alias opencad="cdcad && qexp"
alias openscripts="cdscripts && qexp"
alias openprojects="cdprojects && qexp"
alias opensublime="cdsublime && qexp"
alias openlists="cdlists && qexp"
alias openulb="cdulb && sudo thunar ."
alias openbin="cdbin && sudo thunar ."
alias opencrx="cdcrx && qexp"
alias openarc="cdarc && qexp"
alias openbash="cdbash && qexp"
alias openjava="cdjava && qexp"
alias openpwa="cdpwa && qexp"
alias openedu="cdedu && qexp"
alias openbackup="cdbackup && qexp"
alias openbackups="openbackup"
alias opengl="cdgl && qexp"

# open cs460 graphics folder
alias opengfx="cdgfx && qexp"


# faster cd cmds
alias ..="cd ../"
alias ....="cd ../.."
alias ......="cd ../../.."
# go to a windows folder
function wincd()
{
    drivename="${1}"
    cd "/mnt/${drivename}"
}

# go to windows home folder in C drive
alias echomnt="ls -al /mnt"

# windows compatability    aliases
alias cda="cd /mnt/a"
alias cdb="cd /mnt/b"
alias cdc="cd /mnt/c"
alias cdd="cd /mnt/d"
alias cde="cd /mnt/e"
alias cdf="cd /mnt/f"
alias cdg="cd /mnt/g"
alias cdmnt="cd /mnt"

#═════════════════════════════════════════════════════════#
# CONFIGURATION FILES QUICK.EDIT
#═════════════════════════════════════════════════════════#

function bedit ()
{
    cd $wsl
    sublime .bashrc
}

alias becho="bat ${HOME}/.bashrc"

function zedit ()
{
    cd $wsl
    sublime .zshrc
    #.bash_aliases
}
alias cedit="cd ${HOME}/.config && ls"
alias zecho="bat ${HOME}/.bash_aliases"

alias batedit="cedit && micro ./bat/bat.conf"
alias cdmicro="cd ${HOME}/.config/micro"

function medit ()
{
    cdmicro
    sublime settings.json
}

function shedit ()
{
    cd $HOME
    sublime .shell_exports
}

function bistory ()
{
    cd $wsl
    sublime .bash_history
}

function zistory ()
{
    cd $wsl
    sublime .zsh_history
}

function tistory ()
{
if [[ -z $1 ]]; then
  tail ~/.zsh_history
else
  tail ~/.zsh_history --lines $1
fi
}


alias zsh-update="upgrade_oh_my_zsh"
alias qq="exit"
alias aptall="sudo apt-get update && sudo apt-get upgrade"
alias npmig="sudo npm install -g ${0}"
alias rsbash="bash && . ~/.bashrc"
alias rszsh="zsh && . ~/.zshrc"

function aedit ()
{
    cd $wsl
    sublime .bash_aliases
}

alias aecho="bat ~/.bash_aliases"
alias wecho="bat wox_cmds"

# micro faster
alias mm="micro"

# fast thunar
alias qlexp="sudo thunar ."

# quickly access readme in working directory
function readme ()
{
    if [ -f readme ];then
        bat readme
    fi

    if [ -f readme.\* ];then
        bat readme.\*
    fi

    if [ -f readMe ];then
        bat readMe
    fi

    if [ -f readMe.\* ];then
        bat readMe.\*
    fi

    if [ -f Readme ];then
        bat Readme
    fi

    if [ -f Readme.\* ];then
        bat Readme.\*
    fi

    if [ -f ReadMe ];then
        bat ReadMe
    fi

    if [ -f ReadMe.\* ];then
        bat ReadMe.\*
    fi

    if [ -f README ];then
        bat README
    fi
    if [ -f README.md ];then
        bat README.md
    fi
    if [ -f README.\* ];then
        bat README.\*
    else
        [ -f \*.md ] && bat \*.md || echo  2>/dev/null
    fi


    # ( bat readme 2>/dev/null) ||
    # (bat readMe.* 2>/dev/null) ||
    # (bat Readme.* 2>/dev/null) ||
    # (bat ReadMe.* 2>/dev/null) ||
    # ( bat README.* 2>/dev/null))
}

alias rd="readme"
alias rdm="rd"

# edit windows profile ps1 file
function wedit()
{
    sublime "/mnt/c/Users/jared/Documents/PowerShell/ Microsoft.PowerShell_profile.ps1"
}

# windows powercfg shortcut (powercfg.exe)
alias powercfg="powercfg.exe"po
#═════════════════════════════════════════════════════════#
# GIT
#═════════════════════════════════════════════════════════#

alias gc="git clone ${1}"
alias gommit="git commit -m ${1}"
alias gall="git add *"
alias gadd="git add"
alias gpp="git push"
alias gp="git pull"
alias gstat="git status"
alias gs="git status"
alias gsync="git fetch && git pull && git status"
alias greset="git fetch origin && git reset --hard origin/master -q && git clean -n -f -q"


# find all git repos at path (cmd line argv=$1)
function gfind ()
{

    dirArg="${1}"

    [  -z "$dirArg" ] && (find ./ -name ".git") || (find $dirArg -name ".git")
}

# fast commit when no message is REALLY necessary...
# Default commit message is "fast-commit"
function gcfast()
{
    git add --all
    git add *
    git add .
    git commit -m "fast-commit"
    git push
}

# fast commit when message IS REALLY necessary...
# Commit message takes on first cmd line argument
function gcfastm()
{   git add --all
    git add *
    git add .
    git commit -m "${1}"
    git push
}

function gsyncall()
{
    # original directory
    startingDir="${PWD}"



    # initialize gsyncall
    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m syncing initialized . . .    \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    # sync _SCRIPTS
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m syncing _SCRIPTS . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdscripts
    gsync

    # sync coursework
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m syncing coursework . . .     \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcw
    gsync

    # sync _CAD
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m syncing _CAD . . .           \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcad
    gsync

    # sync _SUBLIME
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m syncing _SUBLIME . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdsublime
    gsync

    # sync _PROJECTS
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m syncing _PROJECTS . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdprojects
    gsync

    # sync _CHROMEEXT
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m syncing _CHROMEEXT . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcrx
    gsync

    # # sync _ROMS
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # echo -e "\e[32m syncing _ROMS . . .          \e[0m"
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # cdroms
    # gsync


    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m syncing complete . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    cd $startingDir
}

function gstatall()
{
    # original directory
    startingDir="${PWD}"

    # initialize gstatall
    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m gstats initialized . . .    \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    # _SCRIPTS status
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m status of _SCRIPTS:          \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdscripts
    gs

    # coursework status
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m status of coursework:        \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcw
    gs

    # _CAD status
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m status of _CAD:              \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcad
    gs

    # _SUBLIME status
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m status of _SUBLIME . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdsublime
    gs

    # _PROJECTS status
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m status of _PROJECTS:         \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdprojects
    gs

    # _CHROMEEXT status
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m status of _CHROMEEXT:        \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcrx
    gs

    # # _ROMS status
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # echo -e "\e[32m status of _ROMS:             \e[0m"
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # cdroms
    # gs

    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m gstats complete  . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"


    # go back to original directory
    cd $startingDir

}

function gpushall()
{
    # original directory
    startingDir="${PWD}"

    # initialize gpushall
    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m pushing initialized . . .    \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    # push _SCRIPTS
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m pushing _SCRIPTS . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdscripts
    gcfast

    # push coursework
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32mpushing coursework . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcw
    gcfast

    # push _CAD
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32mpushing _CAD . . .            \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"

    cdcad
    gcfast

    # push _SUBLIME
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32mpushing _SUBLIME . . .        \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdsublime
    gcfast

    # push _PROJECTS
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m pushing _PROJECTS . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdprojects
    gcfast

    # push _CHROMEEXT
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m pushing _CHROMEEXT . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcrx
    gcfast

    # # push _ROMS
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # echo -e "\e[32mpushing _ROMS . . .           \e[0m"
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # cdroms
    # gcfast

    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m pushing complete . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    cd $startingDir

}

alias gfullsync="gsyncall && gpushall"

# Changes to top-level directory of git repository.
alias gtop="cd \$(git rev-parse --show-toplevel)"

# # Reset repo to current version (Master)
# function greset()
# {
#  echo -e "\n\e[32m ══════════════════════════\e[0m"
#     echo -e "\e[32m resetting repository         \e[0m"
#     echo -e "\n\e[32m ══════════════════════════\e[0m"
#     git branch -D master
#     echo "resetting repository . . . "
#     echo " \n . . . "
#     git checkout master
#     echo " \n . . . "
#     gs
# }


function gsyncselect ()
{
    repoName="${1}"
    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m Syncing ${repoName}  . . .   \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    case $repoName in
        scripts )
        cdscripts && gsync;;
        coursework )
        cdcw && gsync;;
        cad )
        cdcad && gsync;;
        sublime )
        cdsublime && gsync;;
        projects )
        cdprojects && gsync;;
        roms )
        cdroms && gsync;;
    esac
}

function gpushselect ()
{
    repoName="${1}"
    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m Pushing ${repoName}  . . .   \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    case $repoName in
        scripts )
        cdscripts && gcfast;;
        coursework )
        cdcw && gcfast;;
        cad )
        cdcad && gcfast;;
        sublime )
        cdsublime && gcfast;;
        projects )
        cdprojects && gcfast;;
        roms )
        cdroms && gcfast;;
    esac
}

function gfullselect ()
{
    repoName="${1}"
    gsyncselect $repoName
    gpushselect $repoName
}

function gselect ()
{
    # sync, push or status?
    opType="$1"

    # which repo?
    repoName="${2}"

    case $opType in
        sync | fetch | pull )
        gsyncselect $repoName;;
        commit | push )
        gpushselect $repoName;;
        all | full | both )
        gfullselect $repoName;;
    esac
}

# git bucket aliases
alias cdgbucket="cddc && cd _GITBUCKET"
alias cdgb="cdgbucket"
# edit gitbucket config file
function gbucketcfg ()
{
    cdgbucket
    cd .gitbucket
    sublime gitbucket.conf
}

alias gbedit="gbucketcfg"

# clean git repo and recompress (git gc cmd)
alias gclean="git gc"

# lfs cleanup
function gcleanlfs ()
{
    git reflog expire --expire-unreachable=now --all
    git gc --prune=now
}
alias gcleanlfs="gcleanlfs"

# clean all repositories
function gcleanall ()
{

    # original directory
    startingDir="${PWD}"

    # initialize gcleanall
    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m cleaning initialized . . .    \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    # clean _SCRIPTS
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m cleaning _SCRIPTS . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdscripts
    git gc

    # clean coursework
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32mcleaning coursework . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcw
    git gc

    # clean _CAD
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32mcleaning _CAD . . .            \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"

    cdcad
    git gc

    # clean _SUBLIME
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32mcleaning _SUBLIME . . .        \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdsublime
    git gc

    # clean _PROJECTS
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m cleaning _PROJECTS . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdprojects
    git gc

    # clean _CHROMEEXT
    echo -e "\e[32m ══════════════════════════  \e[0m"
    echo -e "\e[32m cleaning _CHROMEEXT . . .      \e[0m"
    echo -e "\e[32m ══════════════════════════  \e[0m"
    cdcrx
    git gc

    # # clean _ROMS
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # echo -e "\e[32mcleaning _ROMS . . .           \e[0m"
    # echo -e "\e[32m ══════════════════════════  \e[0m"
    # cdroms
    # git gc

    echo -e "\n\e[32m ══════════════════════════\e[0m"
    echo -e "\e[32m cleaning complete . . .       \e[0m"
    echo -e "\e[32m ══════════════════════════\n\e[0m"

    cd $startingDir

}

# shortcut to gclonemakebundle script
alias gcmb="gclonemakebundle"


#═════════════════════════════════════════════════════════#
# OTHER
#═════════════════════════════════════════════════════════#

#preferred applications
alias setdefaultapps="exo-preferred-applications"

#backup config files
alias cfgbkp="backup_config_files && cdscripts && cd linux_backup_cfg && qexp"
alias bkpcfg="cfgbkp"

# go to config files
alias cdbackups="cdscripts && cd linux_backup_cfg"
alias cdbackup="cdbackups"

# open url in wayback machine
alias gowayback="wslview \"https://web.archive.org/web/*/${1}\""

# backup wox files
alias backupwox="backup_wox_files"

# go to wox backups
alias cdwox="cdscripts && cd wox_backups"
alias openwox="cdwox && qexp"

# faster quick explorer    alias (qexp)
alias qe="qexp"


#═════════════════════════════════════════════════════════#
# SSH-STUFF
#═════════════════════════════════════════════════════════#

# get ssh status
alias sshstat="service ssh status"

# start ssh server
alias sshstart="sudo service ssh start"

# stop ssh server
alias sshstop="sudo service ssh stop"

# connect to Pixel ssh termux
alias termux="ssh 192.168.1.29 -p 8022"

# adb configure port for ssh over usb
function configSSHUsb()
{
    adb forward tcp:8022 tcp:8022
}

# # ssh into asus linux server
# function sshgo ()
# {

#     case $1 in
#         asus)
#             #echo asus
#             ssh "hltdev@192.168.1.15"
#         ;;
#         surface)
#             #echo "surface"
#             ssh "jared@surface-hltdev"
#         ;;
#         gibbs)
#             ssh "jared.barresi001@gibbs.umb.edu"
#         ;;
#         usb)
#             configSSHUsb
#             ssh localhost -p 8022
#         ;;
#         *)
#             echo "none"
#         ;;
#     esac
# }

#═════════════════════════════════════════════════════════#

# cdup [number of lvls to go back]
function cdup ()
{
    [[ "${1}" =~ ^[1-9][0-9]*$ ]] && { cd $( printf "../%.0s" $(seq 1  "${1}") ); } || cd
}
# Speed up java run
#alias jc="javac"
#alias jv="java"

# clean java project
alias jclean="rm *.class"

# build all java project
alias jbuild="javac *.java"

# quick permissions to 775 recursively
#alias chall="chmod 775 . -R"
function chall ()
{
    # -n:string is not null.
    # -z:string is null, that is, has zero length

    inputCmd="${1}"

    if [ -n "$inputCmd" ];
    then
        # printf "recursive, specify (single) dir"
        sudo chmod 775 $inputCmd -R
        echo "setting permissions . . ."
    else
        # printf "recursive, all dirs"
        sudo chmod 775 * -R
        echo "setting permissions . . ."
    fi

}

# function chall ()
# {
# 	if [[ ! -z $1 ]]; then
# 		if [[ -d $1 ]]; then
# 			echo "Dir"
# 		else
# 			echo "Fil"
# 		fi
# 	else
# 		echo "nothing"
# 	fi
# }

# npm quick search into text file and view
function nodequick()
{
    searchTerm="${1}"
    listsPath="/mnt/c/Users/jared/Documents/_SCRIPTS/_LISTS/node-npm-pkglists"
    fileName="npm-search_query-${searchTerm}_results.txt"
    npm search --long $searchTerm > "${listsPath}/${fileName}"
    micro "${listsPath}/${fileName}"

}



# apt quick search into text file and view
function aptquick()
{
    searchTerm="${1}"
    listsPath="/mnt/c/Users/jared/Documents/_SCRIPTS/_LISTS/linux-apt-pkglists"
    fileName="apt-search_query-${searchTerm}_results.txt"
    sudo apt search $searchTerm > "${listsPath}/${fileName}"
    micro "${listsPath}/${fileName}"

}



#═════════════════════════════════════════════════════════#
# ALIASES (TO TEST)
#═════════════════════════════════════════════════════════#

# Sends stdin to system clipboard
alias clip="xclip -i -selection clipboard"

# tidied echo of path
alias path='echo "$PATH" | tr ":" "\n"'

# cat with highlight
alias cah="pygmentize -g"
# start cpv [copy with a progress bar]
#     alias cpv="rsync -poghb --backup-dir=/tmp/rsync -e /dev/null --progress --"
#  d() {
#    if [[ -n "$1" ]]; then
#        cd "+$1"
#      else
#        dirs -v
#      fi
#    }

# fast untar
alias untar="tar -zxvf "

# Need to generate a random, 20-character password for a new online account? No problem.
# alias getpass="openssl rand -base64 20"

# Downloaded a file and need to test the checksum? We've got that covered too.
alias sha="shasum -a 256 "

# IP/NET/WEB TOOLS
# How many times have you needed to know your external IP address
# and had no idea how to get that info? Yeah, me too.
alias ipe="curl ipinfo.io/ip"

# function ipall ()
# {
#     for
#   hostname -I | awk '{print $arg}' | cut -d ' ' -f1
# }

# Need to know your local IP address?
#alias ipi="ifconfig getifaddr en0"
function ipi ()
{
    ip addr show | grep 'inet ' | awk '{print $2}' | cut -f1 -d'/'
}

alias ipconfig="ipconfig.exe"

# A normal ping will go on forever. We don't want that.Instead, let's limit that to just five pings.
alias ping="ping -c 5"

# Start a web server in any folder you'd like.
alias www="python -m SimpleHTTPServer 8000"

# Want to know how fast your network is? Just download Speedtest-cli and use this    alias.
# You can choose a server closer to your location by using the speedtest-cli --list command.
alias speed="speedtest-cli --server 2406 --simple"

# Want to download something but be able to resume if something goes wrong?
alias wget="wget -c "

#═════════════════════════════════════════════════════════#
# FILE MANAGEMENT
#═════════════════════════════════════════════════════════#

alias fd="fdfind"

# find folders fast
alias fdd="fd -t d "
# find folders by depth
function fddep ()
{
    if [ "$#" -eq  "0" ]
    then
        fd -t d -d 2
    else
        fd -t d -d $1
    fi

}

# find by extension
alias fde="fd -e ${1} "

# find files fast
alias fdf="fd -t f "


# print the path with each directory separated by a newline
alias path="echo -e ${PATH//:/\\n}"

# print the last ten modified files in the specified directory
function last () {
    ls -lt $1 | head
}

# copy a file to the clipboard from the command line

function copyfile ()
{
    cat $1 | xclip -selection clipboard
}

#═════════════════════════════════════════════════════════#
# OTHER
#═════════════════════════════════════════════════════════#

# Finally, let's clear the screen.
alias c="clear"

# go to the last directory you were in
alias back="cd $OLDPWD"

# copy the current working directory to the clipboard
alias cpwd="pwd | xclip -selection clipboard"

# create .url shortcut (link)
function createUrlShortcut ()
{
    if [ "$#" -ne 3 ]; then
        echo "Illegal number of parameters. Usage : createUrlShortcut Name Url FileBaseName"
    fi
    printf "[InternetShortcut]\nURL=$1" > $3.url
}

function aptallfix ()
{
    # updates all programs and fixes all permissions
    # ref - `https://hyperlogos.org/page/Restoring-Permissions-Debian-System`
    sudo apt-get --reinstall install `dpkg --get-selections | grep install | grep -v deinstall | cut -f1`
}


# open xdisplay (usually done from Android in most scenarios for me)
function startx ()
{

    case $1 in
        pixel)
            export DISPLAY=192.168.1.13:0
            export PULSE_SERVER=tcp:192.168.1.13:4712
            metacity & thunar ~/
            echo "done"
        ;;
        surface)
            export DISPLAY=surface-hltdev:0
            export PULSE_SERVER=tcp:surface-hltdev:4712
            #metacity & thunar ~/
            echo "done"
        ;;
        asus)
            export DISPLAY=asus-server:0
            export PULSE_SERVER=tcp:asus-server:4712
            #metacity & thunar ~/
            echo "done"
        ;;
        cancel)
            export DISPLAY=:0
            export PULSE_SERVER=tcp:localhost:4712
            echo "done"
        ;;
    esac

    echo "done"

}

# search for code using ag (silversearcher-ag)
alias fdcode="ag"
alias searchcode="fdcode"

# faster aria2c dl manager access
alias a2c="aria2c"

# make project
function mkproject ()
{
    projectName=$1
    cdprojects
    mkdir $projectName
    cd $projectName
    mkdir "_repositories"
    touch "./_repositories/.gitkeep"
    mkdir "_links"
    touch "./_links/.gitkeep"
    mkdir "_documents"
    touch "./_documents/.gitkeep"
    echo "# ${projectName}   " >> README.md

    case $2 in
        0 )
            # git commit = false
            echo "created new project: ${projectName}"
            la
        ;;
        1 )
            # git commit = true
            cd ../
            git add "${projectName}/*"
            gcfastm "initialized new project - ${projectName}"
            cd "${projectName}"
            la
        ;;
        * )
            # no argument given (git commit = false)
            echo "created new project: ${projectName}"
            la
        ;;
    esac

}

alias colortool="/mnt/c/Users/jared/.ColorTool/ColorTool.exe"


url2html ()
{

    curl -X "GET" "http://fuckyeahmarkdown.com/go/?u=${1}&read=1&preview=1"

}

url2md ()
{

    curl -X "GET" "http://fuckyeahmarkdown.com/go/?u=${1}&read=1&md=1"
    # in progress, to determine readeability (or not)
    # if [[ -z ${2} ]]; then
    #    if [[ ${2} = "r" ]]; then
    #         curl -X "GET" "http://fuckyeahmarkdown.com/go/?u=${1}&read=1&md=1"
    #    else
    #       curl -X "GET" "http://fuckyeahmarkdown.com/go/?u=${1}&read=0&md=1"
    #    fi
    # fi

}


# list all in $PATH variable
function getpath ()
{
    echo $PATH | tr : '\n'
}
alias echopath="getpath"
alias pecho="getpath"


#                                #
# umb-cs341-specific functions   #
#                                #

# open UnoArduSim arduino simulator (was used in cs341
function UnoArduSim ()
{
    '/mnt/c/Program Files (x86)/UnoArduSimV2.7.1/UnoArduSim.exe'
}

function start341vms ()
{
   echo -e "\n\e[32m Starting tutor . . . \e[0m"
   # powershell.exe -c "vmrun.exe -T ws start 'C:\Users\jared\Documents\Virtual\ Machines\tutor\tutor.vmx' nogui"
   powershell.exe -c "cd 'C:\\Users\\jared\\Documents\\Virtual Machines\\tutor'; vmrun.exe -T ws start tutor.vmx nogui;exit"
   echo -e "\n\e[32m Starting tutor-vserver . . . \e[0m"
   powershell.exe -c "cd 'C:\\Users\\jared\\Documents\\Virtual Machines\\tutor-vserver'; vmrun.exe -T ws start tutor-vserver.vmx nogui;exit"
}


function stop341vms ()
{

   echo -e "\n\e[31m Stopping tutor . . . \e[0m"
   # powershell.exe -c "vmrun.exe -T ws start 'C:\Users\jared\Documents\Virtual\ Machines\tutor\tutor.vmx' nogui"
   powershell.exe -c "cd 'C:\\Users\\jared\\Documents\\Virtual Machines\\tutor'; vmrun.exe -T ws stop tutor.vmx nogui;exit"
   echo -e "\n\e[31m Stopping tutor-vserver . . . \e[0m"
   powershell.exe -c "cd 'C:\\Users\\jared\\Documents\\Virtual Machines\\tutor-vserver'; vmrun.exe -T ws stop tutor-vserver.vmx nogui;exit"
}


# start elevated powershell (preview-6) prompt
# Note: opens in new window, dont know how to prevent this
function pwshadmin ()
{
    powershell.exe -c "Start-Process pwsh.exe -Verb runAs"
}

# shut down wsl
function wslshutdown ()
{
    powershell.exe -c "wsl --shutdown;exit;"
    exit
}

#
# incomplete
#
# clean unity package cache
# function unityCacheCleaner ()
# {
#     if [[ ! -f $1 ]]; then
#         echo -e "path to project: \n"
#         read projectName
#     else
#         projectName=$1
#     fi

#     cdunity
#     cd ${projectName}/Library

# }
#


# NOTE: add function to add to alias (this) file using echo

# ovf tool shortcut (windows exe in path)
alias ovftool="ovftool.exe"

# install history
function install-history ()
{
	ag install $HOME/.zsh_history | tr  -d "(.*[0-9]\;" | tr -d ":" | bat -p -l sh --pager=less --paging=never
}

alias everything="Everything.exe"
alias Everything="everything"